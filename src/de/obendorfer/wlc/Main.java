package de.obendorfer.wlc;

import java.util.ArrayList;
import java.util.List;

public class Main {
    private static String textString = "Jede Lebensphase hat ihren ganz eigenen Schwerpunkt Karriere Familie Hausbau Ehrenamt für uns bedeutet WorkLifeBalance dass unsere Mitarbeiterinnen und Mitarbeiter berufliche und private Ziele in Einklang bringen können ihr ganzes Berufsleben lang Als Möglichmacher erarbeiten wir mit ihnen individuelle und verlässliche Lösungen die zu ihnen und ihren Zielen passen";
    private static String codeString = "43/14-3/9-18/5-21/3-22/2-27/1";

  public static void main(String[] args) {
    List<String> codeSplits = new ArrayList<>();
    String result = "";
    for (String string : codeString.split("-")) {
      codeSplits.add(string);
    }
    for (String search : codeSplits) {
      result += returnChar(search);
    }
    System.out.println(result);
  }

  private static String returnText (int wordCount) {
      int wordCounter = 1;
      String result = "";
      for (int i = 0; textString.length() >= i; i++) {
          if (textString.charAt(i) == ' ') {
              wordCounter++;
          }
          if (wordCounter == wordCount) {
              for (int j = i; result.length() <= 26; j++) {
                  if (textString.charAt(j) == ' ') {
                    j++;
                  }
                  result += textString.charAt(j);
              }
              break;
          }

      }
      result.replace(" ", "");
      return result;
  }

  private static char returnWordChar (String text, int letterCount) {
    letterCount--;
    return text.charAt(letterCount);
  }

  private static char returnChar (String search) {
    String[] searchArr = search.split("/");
    String text = returnText(Integer.parseInt(searchArr[0]));
    return returnWordChar(text, Integer.parseInt(searchArr[1]));
  }
}
